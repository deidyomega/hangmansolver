import os

def slimdict(hangmanWord, badguess, goodguess):
    wl = len(hangmanWord)
    dictionary = []
    with open("dict.txt", 'r', encoding='utf-8') as rawdict:
        for word in rawdict:
            word = word[:-1]
            if len(word) == wl:
                dictionary.append(word)

    # while based search

    ## remove words that have bad letters, or words that dont have all of the good letters
    ## Much faster than letter location based search, used to optimize code run

    dictionary = [word for word in dictionary if not any(letter in word for letter in badguess)]
    dictionary = [word for word in dictionary if all(letter in word for letter in goodguess)]
    y = 0
    while y < len(dictionary):
        x = 0
        while x < wl:
            if hangmanWord[x] != "-":
                if hangmanWord[x] != dictionary[y][x]:
                    dictionary.pop(y)
                    if y < 2:
                        y = 0
                    else:
                        y = y - 2
            x = x + 1
        y = y + 1
    return dictionary

def goodguesser(hangmanWord, badguess):
    goodguess = []
    for item in hangmanWord:
        if item != "-":
            goodguess.append(item)

    for gl in goodguess:
        x = 0
        while x < len(badguess):
            if badguess[x] == gl:
                badguess.pop(x)
                if x < 2:
                    x = 0
                else:
                    x = x - 2
            x = x + 1
    return goodguess

def chooser(goodguess, dictionary):
    ## Post Filtered Dictionary
    alphacount = [['a', 0], ['b', 0], ['c', 0], ['d', 0], ['e', 0], ['f', 0],
    ['g', 0], ['h', 0], ['i', 0], ['j', 0], ['k', 0], ['l', 0], ['m', 0],
    ['n', 0], ['o', 0], ['p', 0], ['q', 0], ['r', 0], ['s', 0], ['t', 0],
    ['u', 0], ['v', 0], ['w', 0], ['x', 0], ['y', 0], ['z', 0]]

    ## Remove good guesses, since they need not be guessed again
    x = 0
    while x < len(alphacount):
        for gl in goodguess:
            if gl == alphacount[x][0]:
                alphacount.pop(x)
        x = x + 1

    ## count all the letters in all the words, for best guess
    ## Remove duplicate letters:
    ##      Reason why: The goal is to get as many correct
    ##      guesses, not neccisarly get the answer quickly.
    for word in dictionary:
        word = set(word)
        for item in alphacount:
            if item[0] in word:
                item[1] = item[1] + 1

    alphacount.sort(key=lambda e:e[1], reverse=True)
    return alphacount

def main():
    os.system('cls')
    hangmanWord = input("Hangman Word: ")
    badguess = list(input("Attempted letters: "))
    goodguess = goodguesser(hangmanWord, badguess)
    dictionary = slimdict(hangmanWord, badguess, goodguess)
    alphacount = chooser(goodguess, dictionary)

    if alphacount[0][1] == 0:
        print("No words left, bad word not in dictionary")
    else:
        print("Guess: '{}'".format(alphacount[0][0]))
        print("Number of possible words: {}".format(len(dictionary)))
        if len(dictionary) < 6:
            for item in dictionary:
                print(item, end=", ")
    input()

while True:
    main()
